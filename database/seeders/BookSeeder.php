<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $authors = Author::all();

        Book::factory(10)
            ->create()
            ->each(static function (Book $book) use ($authors) {
                $randomAuthors = $authors->random(random_int(1, 3))->pluck('id')->toArray();
                $book->authors()->sync($randomAuthors);
            });
    }
}
