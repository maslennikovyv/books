<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('Название книги');
            $table->string('ISBN')->unique();
            $table->string('genre')->comment('Жанр');
            $table->string('publisher')->comment('Издательство');
            $table->year('publication')->comment('Год издания');
            $table->smallInteger('pages')->comment('Количество страниц');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
