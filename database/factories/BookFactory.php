<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => ucfirst($this->faker->words(3, true)),
            'ISBN' => $this->faker->unique()->isbn10(),
            'genre' => $this->faker->randomElement([
                'Fantasy',
                'Science Fiction',
                'Dystopian',
                'Adventure',
                'Romance',
            ]),
            'publisher' => $this->faker->company(),
            'publication' => $this->faker->year(),
            'pages' => $this->faker->numberBetween(50, 3031),
        ];
    }
}
