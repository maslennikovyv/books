<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'ISBN' => $this->ISBN,
            'authors' => AuthorResource::collection($this->authors),
            'genre' => $this->genre,
            'publisher' => $this->publisher,
            'publication' => $this->publication,
            'pages' => $this->pages,
        ];
    }
}
