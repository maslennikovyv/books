<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookIndexRequest;
use App\Http\Requests\BookStoreRequest;
use App\Http\Resources\BookResource;
use App\Models\Book;
use App\Services\BookService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BookController extends Controller
{

    /** @var BookService */
    private $bookServices;

    /**
     * BookController constructor.
     * @param BookService $bookServices
     */
    public function __construct(BookService $bookServices)
    {
        $this->bookServices = $bookServices;
    }

    /**
     * Display a listing of the resource.
     *
     * @param BookIndexRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(BookIndexRequest $request)
    {
        /** @var Builder $builder */
        $builder = Book::with('authors');

        $page = $request->get('page', 1);
        $perPage = $request->get('limit', 5);
        $sortBy = $request->get('sort_by', 'id');
        $sortIn = $request->get('sort_in', 'desc');
        $builder->orderBy($sortBy, $sortIn);

        $fromDate = $request->get('from_date');
        if ($fromDate) {
            $builder = $builder->where('updated_at', '>', $fromDate);
        }

        return BookResource::collection(
            $builder->paginate(perPage: $perPage, page: $page)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BookStoreRequest $request
     * @return TransportationResource
     * @throws \Exception
     */
    public function store(BookStoreRequest $request)
    {

        $book = $this->bookServices
            ->create($request->validated());

        return new BookResource($book);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Book $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Book $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Book $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}
