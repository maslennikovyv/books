<?php


namespace App\Services;


use App\Models\Book;
use Illuminate\Support\Facades\DB;

class BookService
{

    /** @var AuthorService */
    private $authorService;

    /**
     * BookService constructor.
     * @param AuthorService $authorService
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    /**
     * @param array $params
     * @return Book
     * @throws \Exception
     */
    public function create(array $params)
    {
        $authors = $params['authors'] ?? [];
        unset($params['authors']);

        DB::beginTransaction();
        try {
            /** @var Book $book */
            $book = Book::updateOrCreate(
                [
                    'ISBN' => $params['ISBN'],
                ],
                [
                    'title' => $params['title'],
                    'genre' => $params['genre'],
                    'publisher' => $params['publisher'],
                    'publication' => $params['publication'],
                    'pages' => $params['pages'],
                ]
            );

            $author_ids = $this->authorService->upsert($authors);
            $book->authors()->sync($author_ids);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return $book;
    }
}
