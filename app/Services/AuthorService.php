<?php


namespace App\Services;


use App\Models\Author;
use Illuminate\Support\Facades\DB;

class AuthorService
{

    /**
     * @param $authors
     * @return array
     * @throws \Exception
     */
    public function upsert(array $authors): array
    {
        $author_ids = [];
        DB::beginTransaction();
        try {
            foreach ($authors as $author) {
                $model = Author::updateOrCreate(
                    [
                        'name' => $author['name'],
                    ],
                    [
                    ]
                );
                $author_ids[] = $model->getKey();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
        return $author_ids;
    }
}
